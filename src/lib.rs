#![warn(clippy::all)]
#![allow(non_snake_case)]

use solclient_sys::*;
use std::ffi::{CStr, CString};
use std::fmt;
use std::slice;
use std::rc::Rc;
use std::convert::TryInto;
use std::{mem, ptr};
use thiserror::Error;
use std::collections::HashMap;

#[derive(Error, Debug)]
pub struct SolClientError {
    pub return_code: solClient_returnCode,
    pub subcode: Option<solClient_subCode>,
    pub response_code: u32,
}

impl SolClientError {
    fn from_return_code(rc: solClient_returnCode) -> Self {
        let errorInfo = unsafe { *solClient_getLastErrorInfo() };

        SolClientError {
            return_code: rc,
            subcode: Some(errorInfo.subCode),
            response_code: errorInfo.responseCode,
        }
    }
}

impl fmt::Display for SolClientError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "ReturnCode: {}; SubCode: {:?}",
            self.return_code, self.subcode
        )
    }
}

#[allow(non_snake_case)]
extern "C" fn session_event_callback<F>(
    session: solClient_opaqueSession_pt,
    event: solClient_session_eventCallbackInfo_pt,
    user: *mut ::std::os::raw::c_void,
) where
    F: FnMut(SolSession, SessionEvent),
{
    let callback_ptr = user as *mut F;
    let callback = unsafe { &mut *callback_ptr };
    let sess = SolSession::from_ptr(session).unwrap();
    let evt = SessionEvent::from_ptr(event).unwrap();

    callback(sess, evt);
}

#[allow(non_snake_case)]
unsafe extern "C" fn message_receive_callback<F>(
    session: solClient_opaqueSession_pt,
    msg: solClient_opaqueMsg_pt,
    user: *mut ::std::ffi::c_void,
) -> solClient_rxMsgCallback_returnCode
where
    F: FnMut(SolSession, SolMsg) -> solClient_rxMsgCallback_returnCode,
{
    let callback_ptr = user as *mut F;
    let callback = &mut *callback_ptr;
    let sess = SolSession::from_ptr(session).unwrap();
    let message = SolMsg::from_ptr(msg);

    callback(sess, message)
}

/// Wrapper for Solace solClient_opaqueContext_pt.
#[derive(Debug)]
pub struct SolContext {
    context_p: solClient_opaqueContext_pt,
}

impl SolContext {
    /// Wrapper for solClient_context_create
    /// Note: ONLY supports Solace-controlled context threads.
    pub fn create() -> Result<SolContext, SolClientError> {
        let mut context_p: solClient_opaqueContext_pt = std::ptr::null_mut();
        let mut contextFuncInfo = solClient_context_createFuncInfo {
            regFdInfo: solClient_context_createRegisterFdFuncInfo {
                regFdFunc_p: None,
                unregFdFunc_p: None,
                user_p: std::ptr::null_mut(),
            },
        };

        let result = unsafe {
            let props = _solClient_contextPropsDefaultWithCreateThread.as_mut_ptr();

            solClient_context_create(
                props,
                &mut context_p,
                &mut contextFuncInfo,
                std::mem::size_of::<solClient_context_createRegisterFdFuncInfo>() as u64,
            )
        };

        match result {
            solClient_returnCode::SOLCLIENT_OK => Ok(SolContext { context_p }),
            _ => Err(SolClientError::from_return_code(result)),
        }
    }
}

/// Wrapper for Solace context properties
pub struct SolContextProperties {
    properties: Vec<String>,
}

#[derive(Debug)]
pub struct SolSession {
    session_p: solClient_opaqueSession_pt,
}

impl SolSession {
    pub fn create<EvF, MRF>(
        session_props: SolSessionProperties,
        context: SolContext,
        event_cb: EvF,
        msg_recvd_cb: MRF,
    ) -> Result<SolSession, SolClientError>
    where
        EvF: FnMut(SolSession, SessionEvent) + 'static,
        MRF: FnMut(SolSession, SolMsg) -> solClient_rxMsgCallback_returnCode + 'static,
    {
        let event_closure = Box::into_raw(Box::new(event_cb));
        let msg_recvd_closure = Box::into_raw(Box::new(msg_recvd_cb));

        let mut sessionFuncInfo = solClient_session_createFuncInfo {
            rxInfo: solClient_session_createRxCallbackFuncInfo {
                callback_p: ptr::null_mut(),
                user_p: ptr::null_mut(),
            },
            eventInfo: solClient_session_createEventCallbackFuncInfo {
                callback_p: Some(session_event_callback::<EvF>),
                user_p: event_closure as *mut _,
            },
            rxMsgInfo: solClient_session_createRxMsgCallbackFuncInfo {
                callback_p: Some(message_receive_callback::<MRF>),
                user_p: msg_recvd_closure as *mut _,
            },
        };

        let mut s_p: solClient_opaqueSession_pt = ptr::null_mut();

        let mut ptrs = session_props.as_ptr_array();

        let p = ptrs.as_mut_ptr();

        let rc = unsafe {
            solClient_session_create(
                p,
                context.context_p,
                &mut s_p,
                &mut sessionFuncInfo,
                std::mem::size_of::<solClient_session_createFuncInfo>() as u64,
            )
        };

        match rc {
            solClient_returnCode::SOLCLIENT_OK => Ok(SolSession { session_p: s_p }),
            _ => Err(SolClientError::from_return_code(rc)),
        }
    }

    pub fn from_ptr(sess: solClient_opaqueSession_pt) -> Result<Self, ()> {
        Ok(SolSession { session_p: sess })
    }

    pub fn connect(&self) -> Result<(), SolClientError> {
        let rc = unsafe { solClient_session_connect(self.session_p) };

        match rc {
            solClient_returnCode::SOLCLIENT_OK => Ok(()),
            _ => Err(SolClientError::from_return_code(rc)),
        }
    }

    pub fn send_msg(&self, msg: &SolMsg) -> Result<(), SolClientError> {
        let rc = unsafe { solClient_session_sendMsg(self.session_p, msg.msg_p) };

        match rc {
            solClient_returnCode::SOLCLIENT_OK => Ok(()),
            _ => Err(SolClientError::from_return_code(rc)),
        }
    }

    pub fn subscribe(&self, flags: u32, topic: &str) -> Result<(), solClient_returnCode_t> {
        let c_topic = CString::new(topic).unwrap();

        let rc =
            unsafe { solClient_session_topicSubscribeExt(self.session_p, flags, c_topic.as_ptr()) };

        match rc {
            solClient_returnCode::SOLCLIENT_OK => Ok(()),
            _ => Err(rc),
        }
    }
}

#[derive(Debug)]
pub struct SolSessionProperties {
    props: Box<HashMap<CString, CString>>,
    // pub properties: Vec<*const std::os::raw::c_char>,
}

impl SolSessionProperties {
    // pub fn new(p: Vec<*const std::os::raw::c_char>) -> SolSessionProperties {
    //     SolSessionProperties { properties: p }
    // }

    pub fn new() -> Self {
        SolSessionProperties {
            props: Box::new(HashMap::new())
        }
    }

    pub fn add(self: &mut Self, key: CString, value: String) {

        let k = CString::new(key).unwrap();
        let v = CString::new(value).unwrap();

        self.props.insert(k, v);
    }

    fn as_ptr_array(&self) -> Vec<*const std::os::raw::c_char> {

        let mut result = Vec::with_capacity((self.props.len()*2) + 1);

        for (key, val) in self.props.iter() {
            std::mem::forget(key);
            std::mem::forget(val);
            result.push(key.as_ptr());
            result.push(val.as_ptr());
        }

        result.push(std::ptr::null());

        result
    }
}

pub fn initialize() -> Result<(), &'static str> {
    let result = unsafe {
        solClient_initialize(
            solClient_log_level::SOLCLIENT_LOG_ERROR,
            std::ptr::null_mut(),
        )
    };

    match result {
        solClient_returnCode::SOLCLIENT_OK => Ok(()),
        solClient_returnCode::SOLCLIENT_FAIL => Err("Initialization Failed."),
        _ => Err("Unexpected result."),
    }
}

#[derive(Debug)]
pub enum Destination {
    Null,
    Topic(CString),
    Queue(CString),
    TopicTemp(CString),
    QueueTemp(CString)
}

impl Destination {

    fn from_ptr(ptr: solClient_destination) -> Self {

        unsafe {
            match ptr.destType {
                solClient_destinationType::SOLCLIENT_NULL_DESTINATION => Destination::Null,
                solClient_destinationType::SOLCLIENT_TOPIC_DESTINATION => Destination::Topic(CStr::from_ptr(ptr.dest).to_owned()),
                solClient_destinationType::SOLCLIENT_QUEUE_DESTINATION => Destination::Queue(CStr::from_ptr(ptr.dest).to_owned()),
                _ => panic!()
            }
        }
    }
}

#[derive(Debug)]
pub struct SolMsg {
    pub msg_p: solClient_opaqueMsg_pt,
}

impl SolMsg {
    pub fn new() -> Result<Self, SolClientError> {
        let mut msg_p: solClient_opaqueMsg_pt = ptr::null_mut();

        let rc = unsafe { solClient_msg_alloc(&mut msg_p) };

        match rc {
            solClient_returnCode::SOLCLIENT_OK => Ok(SolMsg { msg_p }),
            _ => Err(SolClientError::from_return_code(rc)),
        }
    }

    pub fn from_ptr(ptr: solClient_opaqueMsg_pt) -> Self {
        SolMsg { msg_p: ptr }
    }

    pub fn set_destination(
        &self,
        t: solClient_destinationType_t,
        dest: &str,
    ) -> Result<(), SolClientError> {
        let dest_str = CString::new(dest).unwrap();

        let mut dest = solClient_destination {
            destType: t,
            dest: dest_str.as_ptr(),
        };

        let rc = unsafe {
            solClient_msg_setDestination(
                self.msg_p,
                &mut dest,
                mem::size_of::<solClient_destination>() as u64,
            )
        };

        match rc {
            solClient_returnCode::SOLCLIENT_OK => Ok(()),
            _ => Err(SolClientError::from_return_code(rc)),
        }
    }

    pub fn get_destination(&self) -> Result<Destination, SolClientError> {

        let mut dest = solClient_destination_t {
            destType: solClient_destinationType::SOLCLIENT_NULL_DESTINATION,
            dest: ptr::null_mut()
        };

        let rc = unsafe {
            solClient_msg_getDestination(
                self.msg_p,
                &mut dest,
                mem::size_of::<solClient_destination>() as u64,
                )
        };

        match rc {
            solClient_returnCode::SOLCLIENT_OK => {
                Ok(Destination::from_ptr(dest))
            },
            _ => Err(SolClientError::from_return_code(rc)),
        }
    }

    pub fn set_binary_attachment(&self, payload: impl Into<Vec<u8>>) -> Result<(), SolClientError> {
        let p: Vec<u8> = payload.into();

        let result = unsafe {
            solClient_msg_setBinaryAttachment(
                self.msg_p,
                p.as_ptr() as *const std::ffi::c_void,
                p.len() as u32,
            )
        };

        match result {
            solClient_returnCode::SOLCLIENT_OK => Ok(()),
            _ => Err(SolClientError::from_return_code(result)),
        }
    }

    pub fn get_binary_attachment(&self) -> Result<Vec<u8>, SolClientError> {
        let mut buf_ptr: *mut std::ffi::c_void = std::ptr::null_mut();
        let mut size: u32 = 0;

        let rc = unsafe { solClient_msg_getBinaryAttachmentPtr(self.msg_p, &mut buf_ptr, &mut size) };
        let buf = unsafe { slice::from_raw_parts(buf_ptr as *const _, size.try_into().unwrap()) };

        match rc {
            solClient_returnCode::SOLCLIENT_OK => {
                Ok(Vec::from(buf))
            }
            _ => Err(SolClientError::from_return_code(rc)),
        }
    }
}

// impl Drop for SolMsg {

//     fn drop(&mut self) {
//         unsafe {
//             solClient_msg_free(&mut self.msg_p);
//         }
//     }

// }

pub struct SessionEvent {
    evt: solClient_session_eventCallbackInfo,
}

impl SessionEvent {
    pub fn from_ptr(event: solClient_session_eventCallbackInfo_pt) -> Result<Self, ()> {
        Ok(SessionEvent {
            evt: unsafe { *event },
        })
    }
}

impl fmt::Display for SessionEvent {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Event: {:?} (Code: {}",
            self.evt.sessionEvent, self.evt.responseCode
        )
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_initialize() {
        assert!(initialize().is_ok());
    }

    #[test]
    fn test_create_context() {
        assert!(initialize().is_ok());

        match SolContext::create() {
            Ok(_) => assert!(true),
            Err(e) => {
                println!("{:?}", e);
                assert!(false);
            }
        }
    }

    #[test]
    fn test_create_msg() {
        assert!(initialize().is_ok());

        let msg = SolMsg::new();
        assert!(msg.is_ok());
    }

    #[test]
    fn test_msg_set_destination() {
        assert!(initialize().is_ok());

        let msg = SolMsg::new().unwrap();

        let result = msg.set_destination(
            solClient_destinationType::SOLCLIENT_TOPIC_DESTINATION,
            "HELLO",
        );

        assert!(result.is_ok());
    }

    #[test]
    fn test_msg_set_binary_attachment() {
        assert!(initialize().is_ok());
        let msg = SolMsg::new().unwrap();

        let payload = vec![65];

        let result = msg.set_binary_attachment(payload);

        assert!(result.is_ok());

        let att = msg.get_binary_attachment();

        assert!(att.is_ok());
    }

    #[test]
    fn test_session_props() {

        let mut props = SolSessionProperties::new();
        props.add(
            CStr::from_bytes_with_nul(SOLCLIENT_SESSION_PROP_HOST).unwrap().to_owned(),
            String::from("localhost")
        );
        props.add(
            CStr::from_bytes_with_nul(SOLCLIENT_SESSION_PROP_VPN_NAME).unwrap().to_owned(),
            String::from("vpn")
        );
        props.add(
            CStr::from_bytes_with_nul(SOLCLIENT_SESSION_PROP_USERNAME).unwrap().to_owned(),
            String::from("username")
        );
        props.add(
            CStr::from_bytes_with_nul(SOLCLIENT_SESSION_PROP_PASSWORD).unwrap().to_owned(),
            String::from("password")
        );

        let ptr_arr = props.as_ptr_array();

        dbg!(&ptr_arr);

        unsafe {
            match CStr::from_ptr(ptr_arr[0]).to_str().unwrap() {
                "SESSION_VPN_NAME" => assert!(CStr::from_ptr(ptr_arr[1]).to_str().unwrap() == "vpn".to_string()),
                "SESSION_USERNAME" => assert!(CStr::from_ptr(ptr_arr[1]).to_str().unwrap() == "username".to_string()),
                "SESSION_HOST" => assert!(CStr::from_ptr(ptr_arr[1]).to_str().unwrap() == "localhost".to_string()),
                "SESSION_PASSWORD" => assert!(CStr::from_ptr(ptr_arr[1]).to_str().unwrap() == "password".to_string()),
                _ => assert!(true)
            }
        }

    }
}
