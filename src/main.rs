use clap::{App, Arg, SubCommand};
use solclient::*;
use solclient_sys::solClient_destinationType;
use solclient_sys::*;
use std::collections::HashMap;
use std::convert::From;
use std::ffi::CStr;
use std::ffi::CString;

#[derive(Debug)]
pub enum AppError {
    SolClientError(SolClientError),
    AppError,
}

impl From<SolClientError> for AppError {
    fn from(value: SolClientError) -> Self {
        AppError::SolClientError(value)
    }
}

fn main() -> Result<(), AppError> {
    let matches = App::new("SolClientSample")
        .version("1.0")
        .arg(
            Arg::with_name("host")
                .short("h")
                .takes_value(true)
                .default_value("localhost"),
        )
        .arg(
            Arg::with_name("vpn_name")
                .short("n")
                .takes_value(true)
                .default_value("default"),
        )
        .arg(
            Arg::with_name("username")
                .short("u")
                .takes_value(true)
                .default_value("default"),
        )
        .arg(
            Arg::with_name("password")
                .short("p")
                .takes_value(true)
                .default_value("default"),
        )
        .subcommand(
            SubCommand::with_name("listen").arg(
                Arg::with_name("topic")
                    .short("t")
                    .required(true)
                    .multiple(true)
                    .takes_value(true),
            ),
        )
        .subcommand(
            SubCommand::with_name("send")
                .arg(
                    Arg::with_name("topic")
                        .short("t")
                        .required(true)
                        .multiple(true)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("msg")
                        .short("m")
                        .required(true)
                        .takes_value(true),
                ),
        )
        .get_matches();

    if let Ok(()) = initialize() {
        let ctx = SolContext::create()?;

        let mut props = SolSessionProperties::new();

        props.add(
            CStr::from_bytes_with_nul(SOLCLIENT_SESSION_PROP_HOST).unwrap().to_owned(),
            String::from("localhost")
        );
        props.add(
            CStr::from_bytes_with_nul(SOLCLIENT_SESSION_PROP_VPN_NAME).unwrap().to_owned(),
            matches.value_of("vpn_name").unwrap().to_string()
        );
        props.add(
            CStr::from_bytes_with_nul(SOLCLIENT_SESSION_PROP_USERNAME).unwrap().to_owned(),
            matches.value_of("username").unwrap().to_string()
        );
        props.add(
            CStr::from_bytes_with_nul(SOLCLIENT_SESSION_PROP_PASSWORD).unwrap().to_owned(),
            matches.value_of("password").unwrap().to_string()
        );

        let on_event = |session: SolSession, event: SessionEvent| {
            println!("Got session event");
        };

        let on_msg = |session, msg: SolMsg| -> solClient_rxMsgCallback_returnCode {
            unsafe {
                solClient_msg_dump(msg.msg_p, std::ptr::null_mut(), 0);
            }

            if let Ok(destination) = msg.get_destination() {
                if let Destination::Topic(topic) = destination {
                    println!("Topic: {}", topic.to_str().unwrap());
                }
            } else {
            }

            if let Ok(payload) = msg.get_binary_attachment() {
                println!("{:?}", payload);
            } else {
                println!("Couldn't get attachment.");
            }
            solClient_rxMsgCallback_returnCode::SOLCLIENT_CALLBACK_OK
        };

        let sess_r = SolSession::create(props, ctx, on_event, on_msg);

        dbg!(&sess_r);

        let sess = sess_r.unwrap();

        sess.connect()?;

        if matches.is_present("listen") {
            println!("SolClient Listen...");
            println!("Subscribing to topics...");
            if let Some(listen_args) = matches.subcommand_matches("listen") {
                for topic in listen_args.values_of("topic").unwrap() {
                    println!("{}", topic);
                    let res = sess.subscribe(0x02, topic);
                    if res.is_err() {
                        println!("Oops: {:?}", res);
                    }
                }
                loop {
                    std::thread::park_timeout(std::time::Duration::new(5, 0));
                }
            }
            Ok(())
        } else if matches.is_present("send") {
            println!("SolClient Send...");
            let msg = SolMsg::new()?;

            if let Some(send_args) = matches.subcommand_matches("send") {
                msg.set_destination(
                    solClient_destinationType::SOLCLIENT_TOPIC_DESTINATION,
                    send_args.value_of("topic").unwrap(),
                )?;
                println!("Destination set.");

                msg.set_binary_attachment(send_args.value_of("msg").unwrap())?;
                println!("Payload set.");
            }

            sess.send_msg(&msg).unwrap();

            Ok(())
        } else {
            Err(AppError::AppError)
        }
    } else {
        println!("Could not initialize Solace API.");
        panic!();
    }
}
